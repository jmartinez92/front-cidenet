import Template from "./components/template";
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <Router>
      <Template />
    </Router>
  );
}

export default App;
