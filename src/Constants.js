const API_HOST_DEV = "http://localhost:8080";
const API_HOST_PROD = "http://localhost:8080";

const dev = {
  API_URL_EMPLOYEE: API_HOST_DEV + "/employees",
};
const prod = {
  API_URL_EMPLOYEE: API_HOST_PROD + "/employees",
};

export default process.env.NODE_ENV === "development" ? dev : prod;
