import config from "../Constants";

const getAll = async (filters) => {
  return await fetch(`${config.API_URL_EMPLOYEE}?${filters}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
};

const getById = async (id) => {
  return await fetch(`${config.API_URL_EMPLOYEE}/${id}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
};

const create = async (data) => {
  return await fetch(config.API_URL_EMPLOYEE, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
};

const edit = async (id, data) => {
  return await fetch(`${config.API_URL_EMPLOYEE}/${id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
};

export default { getAll, getById, create, edit };
