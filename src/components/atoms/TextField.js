import React from "react";
import { TextField } from "@material-ui/core";

const Field = ({ name, label, ...props }) => {
  return (
    <TextField
      fullWidth
      size="medium"
      variant="outlined"
      InputLabelProps={{
        shrink: true,
      }}
      name={name}
      label={label}
      {...props}
    />
  );
};

export default Field;
