import React from "react";
import { Button, makeStyles } from "@material-ui/core";

const styles = makeStyles(() => ({
  button: {
    borderRadius: "15px",
  },
}));

const Index = ({
  children,
  color = "default",
  variant = "contained",
  textColor = "",
  startIcon,
  ...props
}) => {
  const classes = styles();
  return (
    <Button
      fullWidth
      variant={variant}
      color={color}
      className={classes.button}
      startIcon={startIcon}
      style={{ color: textColor }}
      size="large"
      {...props}
    >
      {children}
    </Button>
  );
};

export default Index;
