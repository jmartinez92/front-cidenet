import React from "react";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";
import PropTypes from "prop-types";

const AlertMessage = ({ severity, message, open, ...props }) => {
  React.useEffect(() => {
    const timer = setTimeout(() => {
      props.setAlertStatus((prevStatus) => ({
        ...prevStatus,
        open: false,
      }));
    }, 3000);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  return (
    <div>
      <Collapse in={true} style={{ position: "fixed", top: "80px" }}>
        <Alert severity={severity}>{message}</Alert>
      </Collapse>
    </div>
  );
};

AlertMessage.propTypes = {
  severity: PropTypes.oneOf(["error", "warning", "info", "success"]),
};

export default AlertMessage;
