import React from "react";
import PropTypes from "prop-types";
import { DataGrid } from "@material-ui/data-grid";

const DataTable = ({
  columns = [],
  rows = [],
  pageSize,
  rowCount,
  handlePage,
  ...props
}) => {
  const handlePageChange = (params) => {
    handlePage(params.page);
  };

  return (
    <>
      <DataGrid
        rows={rows}
        columns={columns}
        disableColumnMenu
        paginationMode="server"
        onPageChange={handlePageChange}
        pageSize={pageSize}
        rowCount={rowCount}
      />
    </>
  );
};

DataTable.propTypes = {
  columns: PropTypes.array.isRequired,
};

export default DataTable;
