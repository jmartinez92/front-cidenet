import React from "react";
import { useForm, Controller } from "react-hook-form";
import TextField from "../../atoms/TextField";
import Select from "../../molecules/Select";
import ToolBar from "../../molecules/ToolBar";
import KeyboardDatePicker from "../../molecules/KeyboardDatePicker";
import { makeStyles, Grid, Paper } from "@material-ui/core";
import EmployeeService from "../../../services/EmployeeService";
import { useParams } from "react-router-dom";

const documentTypes = [
  { value: "CC", label: "Cédula de ciudania" },
  { value: "CE", label: "Cédula de extranjería" },
  { value: "PA", label: "Pasaporta" },
  { value: "PE", label: "Permiso especial" },
];

const countries = [
  { value: "169", label: "Colombia" },
  { value: "249", label: "Estados Unidos" },
];

const deparments = [
  { value: 1, label: "Administración" },
  { value: 2, label: "Compras" },
  { value: 3, label: "Financiera" },
  { value: 4, label: "Infraestructra" },
  { value: 5, label: "Operación" },
  { value: 6, label: "Servicios varios" },
  { value: 7, label: "Talento humano" },
];

const styles = makeStyles((theme) => ({
  paperContainer: {
    borderTopRightRadius: "10px",
    borderTopLeftRadius: "10px",
    height: "59.3vh",
    overflow: "auto",
    padding: theme.spacing(4),
    marginTop: theme.spacing(1),
  },
}));

const Employee = (props) => {
  const classes = styles();
  let { id } = useParams();
  const { control, errors, reset, handleSubmit, getValues } = useForm();

  React.useEffect(() => {
    const fecth = async () => {
      const response = await EmployeeService.getById(id);
      const json = await response.json();
      const data = json.data;
      if (json.status === 200) {
        reset({
          id: data.id,
          surname: !data.surname ? "" : data.surname,
          secondSurname: !data.secondSurname ? "" : data.secondSurname,
          firstName: !data.firstName ? "" : data.firstName,
          otherNames: !data.otherNames ? "" : data.otherNames,
          documentType: !data.documentType ? "" : data.documentType,
          documentNumber: !data.documentNumber ? "" : data.documentNumber,
          country: !data.country ? "" : data.country,
          email: !data.email ? "" : data.email,
          department: !data.department ? "" : data.department,
          status: data.status,
          admissionDate: data.admissionDate,
        });
      }
    };
    fecth();
  }, []);

  const editEmployee = async (employee) => {
    const response = await EmployeeService.edit(id, employee);
    const data = await response.json();
    if (data.status === 200) {
      props.setAlertStatus({
        open: true,
        severity: "success",
        message: "Registro creado correctamente",
      });
      props.history.push("/employees");
    } else {
      props.setAlertStatus({
        open: true,
        severity: "error",
        message: data.message,
      });
    }
  };

  const onSubmit = (data) => {
    editEmployee(data);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <ToolBar title="Nuevo empleado" redirect="employees" />
        <Paper className={classes.paperContainer}>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <TextField
                    label="Primer apellido"
                    onChange={onChange}
                    value={value}
                    error={errors.surname ? true : false}
                    helperText={errors.surname?.message}
                  />
                )}
                name="surname"
                defaultValue=""
                rules={{
                  required: true,
                  validate: (value) => value.trim().length !== 0,
                  maxLength: 20,
                  pattern: {
                    value: /^[A-Z\s]*$/,
                    message: "Permitido sólo letras en mayúsculas",
                  },
                }}
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <TextField
                    label="Segundo apellido"
                    onChange={onChange}
                    value={value}
                    error={errors.secondSurname ? true : false}
                    helperText={errors.secondSurname?.message}
                  />
                )}
                name="secondSurname"
                defaultValue=""
                rules={{
                  required: true,
                  validate: (value) => value.trim().length !== 0,
                  maxLength: 20,
                  pattern: {
                    value: /^[A-Z\s]*$/,
                    message: "Permitido sólo letras en mayúsculas",
                  },
                }}
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <TextField
                    label="Primer nombre"
                    onChange={onChange}
                    value={value}
                    error={errors.firstName ? true : false}
                    helperText={errors.firstName?.message}
                  />
                )}
                name="firstName"
                defaultValue=""
                rules={{
                  required: true,
                  validate: (value) => value.trim().length !== 0,
                  maxLength: 20,
                  pattern: {
                    value: /^[A-Z\s]*$/,
                    message: "Permitido sólo letras en mayúsculas",
                  },
                }}
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <TextField
                    label="Otros nombres"
                    onChange={onChange}
                    value={value}
                    error={errors.otherNames ? true : false}
                    helperText={errors.otherNames?.message}
                  />
                )}
                name="otherNames"
                defaultValue=""
                rules={{
                  maxLength: 50,
                  pattern: {
                    value: /^[A-Z\s]*$/,
                    message: "Permitido sólo letras en mayúsculas",
                  },
                }}
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <Select
                    label="Tipo de identificación"
                    onChange={onChange}
                    value={value}
                    items={documentTypes}
                    error={errors.documentType ? true : false}
                  />
                )}
                name="documentType"
                defaultValue=""
                rules={{
                  validate: (value) =>
                    value.trim().length !== 0 ||
                    (value.trim().length === 0 &&
                      getValues("documentNumber").trim().length === 0),
                }}
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <TextField
                    label="Número de identificación"
                    onChange={onChange}
                    value={value}
                    error={errors.documentNumber ? true : false}
                    helperText={errors.documentNumber?.message}
                  />
                )}
                name="documentNumber"
                defaultValue=""
                rules={{
                  maxLength: 20,
                  validate: (value) =>
                    value.trim().length !== 0 ||
                    (value.trim().length === 0 &&
                      getValues("documentType").trim().length === 0),
                  pattern: {
                    value: /^[A-Za-z\-0-9]*$/,
                    message:
                      "Permitido sólo conjunto de caracteres (a-z/A-Z/0-9/-)",
                  },
                }}
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <Select
                    label="Pais"
                    onChange={onChange}
                    value={value}
                    items={countries}
                    error={errors.country ? true : false}
                  />
                )}
                name="country"
                defaultValue=""
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <TextField
                    label="Correo electrónico"
                    onChange={onChange}
                    value={value}
                    disabled
                  />
                )}
                name="email"
                defaultValue=""
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <KeyboardDatePicker
                    label="Fecha de ingreso"
                    onChange={onChange}
                    value={value}
                    format="dd/MM/yyyy"
                    variant="inline"
                    disableFuture
                  />
                )}
                name="admissionDate"
                defaultValue={null}
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <Select
                    label="Ärea"
                    onChange={onChange}
                    value={value}
                    items={deparments}
                  />
                )}
                name="department"
                defaultValue=""
                control={control}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ onChange, value }) => (
                  <Select
                    label="Estado"
                    onChange={onChange}
                    value={value}
                    items={[
                      { value: 1, label: "Activo" },
                      { value: 0, label: "Inactivo" },
                    ]}
                  />
                )}
                name="status"
                defaultValue=""
                control={control}
              />
            </Grid>
          </Grid>
        </Paper>
      </form>
    </>
  );
};

export default Employee;
