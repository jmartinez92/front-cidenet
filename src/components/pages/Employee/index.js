import React from "react";
import { makeStyles, Paper } from "@material-ui/core";
import ToolBar from "../../molecules/ToolBarIndex";
import DataTable from "../../organisms/Table";
import EmployeeService from "../../../services/EmployeeService";

const styles = makeStyles((theme) => ({
  paperContainer: {
    borderTopRightRadius: "10px",
    borderTopLeftRadius: "10px",
    height: "59.3vh",
    overflow: "auto",
    padding: theme.spacing(4),
    marginTop: theme.spacing(1),
  },
}));

const columns = [
  {
    field: "id",
    headerName: "Id",
    width: 70,
    renderCell: (params) => {
      const url = `/employees/edit/${params.value}`;
      return <a href={url}>{params.value}</a>;
    },
  },
  { field: "surname", headerName: "Primer apellido", width: 150 },
  { field: "secondSurname", headerName: "Segundo apellido", width: 170 },
  { field: "firstName", headerName: "Primer nombre", width: 150 },
  { field: "otherNames", headerName: "Otros nombres", width: 150 },
  { field: "documentType", headerName: "Tipo de identificación", width: 200 },
  {
    field: "documentNumber",
    headerName: "Número de identificación",
    width: 220,
  },
  { field: "email", headerName: "Correo electrónico", width: 200 },
  { field: "countryName", headerName: "Pais", width: 100 },
  { field: "departmentName", headerName: "Área", width: 100 },
];

const Employee = (props) => {
  const classes = styles();
  const pageSize = 10;
  const [data, setData] = React.useState({
    content: [],
    rowCount: 0,
  });
  const [page, setPage] = React.useState(0);

  const handlePage = (page) => {
    setPage(page);
  };

  const handleLoad = async (page) => {
    const filters = `page=${page}&size=${pageSize}`;
    const response = await EmployeeService.getAll(filters);
    return await response.json();
  };

  React.useEffect(() => {
    (async () => {
      const response = await handleLoad(page);
      const data = response.data;
      setData({
        content: data.content,
        rowCount: data.totalElements,
      });
    })();
  }, [page]);

  return (
    <>
      <ToolBar title="Empleados" redirect="employees/new" />
      <Paper className={classes.paperContainer}>
        <DataTable
          columns={columns}
          rows={data.content}
          pageSize={pageSize}
          rowCount={data.rowCount}
          handlePage={handlePage}
        />
      </Paper>
    </>
  );
};

export default Employee;
