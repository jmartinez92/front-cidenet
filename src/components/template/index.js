import React from "react";
import { makeStyles } from "@material-ui/core";
import Header from "./Header";
import Main from "./Main";

const styles = makeStyles(() => ({
  root: {
    backgroundColor: "#E7E7E7",
  },
}));

const Template = () => {
  const classes = styles();

  return (
    <div className={classes.root}>
      <Header />
      <Main />
    </div>
  );
};

export default Template;
