import React from "react";
import { Container, makeStyles } from "@material-ui/core";
import { Switch, Route } from "react-router-dom";
import Alert from "../atoms/Alert";
import Employee from "../pages/Employee";
import NewEmployee from "../pages/Employee/New";
import EditEmployee from "../pages/Employee/Edit";

const styles = makeStyles((theme) => ({
  content: {
    height: "100vh",
    position: "absolute",
    top: "0",
    width: "100%",
  },
  container: {
    height: "80vh",
    marginTop: "20vh",
    [theme.breakpoints.up("lg")]: {
      paddingLeft: theme.spacing(12),
      paddingRight: theme.spacing(12),
    },
    [theme.breakpoints.only("md")]: {
      paddingLeft: theme.spacing(9),
      paddingRight: theme.spacing(9),
    },
  },
}));

const Main = (props) => {
  const classes = styles();

  const [alertStatus, setAlertStatus] = React.useState({
    open: false,
    severity: "success",
    message: "",
  });

  return (
    <main className={classes.content}>
      <Container className={classes.container} maxWidth="lg">
        {alertStatus.open && (
          <Alert
            severity={alertStatus.severity}
            message={alertStatus.message}
            setAlertStatus={setAlertStatus}
          />
        )}
        <Switch>
          <Route path="/employees" component={Employee} exact />
          <Route
            path="/employees/new"
            render={(props) => (
              <NewEmployee {...props} setAlertStatus={setAlertStatus} />
            )}
            exact
          />
          <Route
            path="/employees/edit/:id"
            render={(props) => (
              <EditEmployee {...props} setAlertStatus={setAlertStatus} />
            )}
            exact
          />
        </Switch>
      </Container>
    </main>
  );
};

export default Main;
