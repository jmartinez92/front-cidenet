import React from "react";
import { makeStyles, Paper } from "@material-ui/core";

const styles = makeStyles(() => ({
  papper: {
    background:
      "linear-gradient(to right, #1976D2 10%, #008B91 80%, #008B91 100%)",
    height: "45vh",
  },
}));

const Header = (props) => {
  const classes = styles();
  return (
    <>
      <Paper className={classes.papper} square />
    </>
  );
};

export default Header;
