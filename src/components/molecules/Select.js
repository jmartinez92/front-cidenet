import React from "react";
import { MenuItem } from "@material-ui/core";
import TextField from "../atoms/TextField";

const Select = ({ label, name, items = [], ...props }) => {
  return (
    <TextField select label={label} name={name} {...props}>
      {items.map((items) => (
        <MenuItem key={items.value} value={items.value}>
          {items.label}
        </MenuItem>
      ))}
    </TextField>
  );
};

export default Select;
