import React from "react";
import Button from "../atoms/Button";
import { Grid, makeStyles, Paper, Typography } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";

const styles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  papper: {
    borderRadius: "10px",
    padding: "10px 32px",
    background:
      "linear-gradient(to right, #1976D2 10%, #008B91 80%, #008B91 100%)",
  },
  typography: {
    color: "#fff",
    marginTop: "8px",
  },
}));

const ToolBar = ({ title, redirect }) => {
  const history = useHistory();
  const classes = styles();
  return (
    <div className={classes.toolbar}>
      <Paper elevation={24} className={classes.papper}>
        <Grid container spacing={1}>
          <Grid item xs={6} merge="dense">
            <Typography variant="h5" className={classes.typography}>
              {title}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Grid container spacing={1} justify="flex-end">
              <Grid item>
                <Button
                  startIcon={<AddIcon />}
                  textColor="#4caf50"
                  onClick={() => history.push(`/${redirect}`)}
                >
                  Nuevo
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

export default ToolBar;
