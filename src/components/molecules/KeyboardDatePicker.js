import React from "react";
import DateFnsUtils from "@date-io/date-fns";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";

const DatePicker = ({ label, name, format, variant, ...props }) => {
  return (
    <>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          autoOk
          disableToolbar
          fullWidth
          size="medium"
          variant={variant}
          inputVariant="outlined"
          label={label}
          name={name}
          format={format}
          InputLabelProps={{
            shrink: true,
          }}
          {...props}
        />
      </MuiPickersUtilsProvider>
    </>
  );
};

export default DatePicker;
